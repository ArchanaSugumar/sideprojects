var builder = require('botbuilder');
module.exports=[
function(session){
    var card = {
        'contentType': 'application/vnd.microsoft.card.adaptive',
        'content': {
            '$schema': 'http://adaptivecards.io/schemas/adaptive-card.json',
            'type': 'AdaptiveCard',
            'version': '1.0',
            'body': [
                {
                    'type': 'Container',
                    'speak': '<s>Hello There!</s><s>Below is the link for complete holiday list.</s>',
                    'items': [
                        {
                            'type': 'ColumnSet',
                            'columns': [
                                {
                                    'type': 'Column',
                                    'size': 'auto',
                                    'items': [
                                        {
                                            'type': 'Image',
                                            'url': 'https://mjkretsinger.com/wp-content/uploads/2014/01/client-logo-deluxe.jpg',
                                            'size': 'medium',
                                            'style': 'person'
                                        }
                                    ]
                                },
                                {
                                    'type': 'Column',
                                    'size': 'stretch',
                                    'items': [
                                        {
                                            'type': 'TextBlock',
                                            'text': 'Hello There!',
                                            'weight': 'bolder',
                                            'isSubtle': true
                                        },
                                        {
                                            'type': 'TextBlock',
                                            'text': 'Below is the link for complete holiday list.',
                                            'wrap': true
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            'actions': [
                
                {
                    'type': 'Action.ShowCard',
                    'title': 'Complete Holiday List',
                    'speak': '<s>Complete Holiday List</s>',
                    'card': {
                        'type': 'AdaptiveCard',
                        'body': [],                           
                        'actions': [
                            {
                                'type': 'Action.OpenUrl',
                                'title': 'Link to Complete Holiday List',
                                "url": "https://deluxe.atlassian.net/wiki/display/FSDEV/calendar/ac1c53a4-85a2-4519-b651-db74940bfee3?calendarName=Holidays"
                            }
                        ]
                    }
                }    
            ]
        }
    };
    var msg = new builder.Message(session)
        .addAttachment(card);
    session.send(msg);
}];
