var builder = require('botbuilder');
module.exports=[
function(session){
    var card = {
        'contentType': 'application/vnd.microsoft.card.adaptive',
        'content': {
            '$schema': 'http://adaptivecards.io/schemas/adaptive-card.json',
            'type': 'AdaptiveCard',
            'version': '1.0',
            'body': [
                {
                    'type': 'Container',
                    'speak': '<s>Hello There!</s><s>Below List will help you know more.</s>',
                    'items': [
                        {
                            'type': 'ColumnSet',
                            'columns': [
                                {
                                    'type': 'Column',
                                    'size': 'auto',
                                    'items': [
                                        {
                                            'type': 'Image',
                                            'url': 'https://mjkretsinger.com/wp-content/uploads/2014/01/client-logo-deluxe.jpg',
                                            'size': 'medium',
                                            'style': 'person'
                                        }
                                    ]
                                },
                                {
                                    'type': 'Column',
                                    'size': 'stretch',
                                    'items': [
                                        {
                                            'type': 'TextBlock',
                                            'text': 'Hello There!',
                                            'weight': 'bolder',
                                            'isSubtle': true
                                        },
                                        {
                                            'type': 'TextBlock',
                                            'text': 'Below List will help you know more.',
                                            'wrap': true
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            'actions': [
                {
                    'type': 'Action.ShowCard',
                    'title': 'Team Lead CheckLists',
                    'speak': '<s>Team Lead CheckLists</s>',
                    'card': {
                        'type': 'AdaptiveCard',
                        'body': [],
                        'actions': [
                            {
                                'type': 'Action.ShowCard',
                                'title': 'Team Lead Checklist(Agile Practice)',
                                'speak': '<s>Team Lead Checklist(Agile Practice)</s>',
                                'card': {
                                'type': 'AdaptiveCard',
                                'body': [],
                                'actions': [
                                             {
                                                'type': 'Action.OpenUrl',
                                                'title': 'Link to Team Lead Checklist(Agile Practice)',
                                                "url":"https://deluxe.atlassian.net/wiki/spaces/FSDEV/pages/236749094/Team+Lead+Checklist+Agile+Practice"
                                             }
                                             ]
                                 }
                                
                                 },
                                 {
                                    'type': 'Action.ShowCard',
                                    'title': 'Team Lead Checklist(DevOps)',
                                    'speak': '<s>Team Lead Checklist(DevOps)</s>',
                                    'card': {
                                    'type': 'AdaptiveCard',
                                    'body': [],
                                    'actions': [
                                                 {
                                                    'type': 'Action.OpenUrl',
                                                    'title': 'Link to Team Lead Checklist(DevOps)',
                                                    "url":"https://deluxe.atlassian.net/wiki/spaces/FSDEV/pages/267190915/Team+Lead+Checklist+DevOps"
                                                 }
                                                 ]
                                     }
                                    
                                     },
                                     {
                                        'type': 'Action.ShowCard',
                                        'title': 'Team Lead Checklist(UX Team)',
                                        'speak': '<s>Link to Team Lead Checklist(UX Team)</s>',
                                        'card': {
                                        'type': 'AdaptiveCard',
                                        'body': [],
                                        'actions': [
                                                     {
                                                        'type': 'Action.OpenUrl',
                                                        'title': 'Team Lead Checklist(UX Team)',
                                                        "url":"https://deluxe.atlassian.net/wiki/spaces/FSDEV/pages/75628905/Team+Lead+Checklist+UX+Team"
                                                        
                                                     }
                                                     ]
                                                }
                                        
                                         },
                                         {
                                            'type': 'Action.ShowCard',
                                            'title': 'Termination Checklist(QA/Development Manager List)',
                                            'speak': '<s>Link to Termination Checklist(QA/Development Manager List)</s>',
                                            'card': {
                                            'type': 'AdaptiveCard',
                                            'body': [],
                                            'actions': [
                                                         {
                                                            'type': 'Action.OpenUrl',
                                                            'title': 'Termination Checklist(QA/Development Manager List)',
                                                            "url":"https://deluxe.atlassian.net/wiki/spaces/FSDEV/pages/147750913/Termination+Checklist+Development+QA+Engineer+-+Manager+Checklist"
                                                         }
                                                         ]
                                                    }
                                            
                                             }        
                                ]
                    }
                }
            ]
        }
    };
    
    var msg = new builder.Message(session)
        .addAttachment(card);
    session.send(msg);
}];
