var builder = require('botbuilder');
module.exports=[
    (session,args,next)=>{
        session.send("Hello There ! I am Eva your HR bot.I can answer your HR related queries");
        builder.Prompts.choice(session, "Please select one of the options:",['New Joinee Documentation List','Complete Holiday List','Other CheckLists','Benefits - Medical &Dental','Apply for Leave request','How to participate in 401K plan','View Paycheque Online','Enroll in stock purchase plan'],{
            retryPrompt: "Invalid choice, Please pick below listed choices",
        listStyle: builder.ListStyle.button,
        maxRetries: 3
        });
    
    },
    function (session, results) {
        var destination = results.response.entity;
        switch (destination)
        {
                case 'New Joinee Documentation List':
                   // session.send("You are looking for new document list for new joinee.Please refer to the link below");
                   // session.send("https://deluxe.atlassian.net/wiki/spaces/FSDEV/pages/74810023/Onboarding+Checklist+-+Template");
                   var card = {
                    'contentType': 'application/vnd.microsoft.card.adaptive',
                    'content': {
                        '$schema': 'http://adaptivecards.io/schemas/adaptive-card.json',
                        'type': 'AdaptiveCard',
                        'version': '1.0',
                        'body': [
                            {
                                'type': 'Container',
                                'speak': '<s>Hello There!</s><s>Are you looking for New Joinee Documentation List?</s>',
                                'items': [
                                    {
                                        'type': 'ColumnSet',
                                        'columns': [
                                            {
                                                'type': 'Column',
                                                'size': 'auto',
                                                'items': [
                                                    {
                                                        'type': 'Image',
                                                        'url': 'https://mjkretsinger.com/wp-content/uploads/2014/01/client-logo-deluxe.jpg',
                                                        'size': 'medium',
                                                        'style': 'person'
                                                    }
                                                ]
                                            },
                                            {
                                                'type': 'Column',
                                                'size': 'stretch',
                                                'items': [
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Hello There!',
                                                        'weight': 'bolder',
                                                        'isSubtle': true
                                                    },
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Are you looking for New Joinee Documentation List?',
                                                        'wrap': true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        'actions': [
                            
                            {
                                'type': 'Action.ShowCard',
                                'title': 'New Joinee Documentation',
                                'speak': '<s>New Joinee Documentatio</s>',
                                'card': {
                                    'type': 'AdaptiveCard',
                                    'body': [],                           
                                    'actions': [
                                        {
                                            'type': 'Action.OpenUrl',
                                            'title': 'Link to New Joinee Documentation List',
                                            "url": "https://deluxe.atlassian.net/wiki/spaces/FSDEV/pages/74810023/Onboarding+Checklist+-+Template"
                                        }
                                    ]
                                }
                            }    
                        ]
                    }
                };
                var msg = new builder.Message(session)
                    .addAttachment(card);
                session.send(msg);
                break;
                case 'Complete Holiday List':
                var card = {
                    'contentType': 'application/vnd.microsoft.card.adaptive',
                    'content': {
                        '$schema': 'http://adaptivecards.io/schemas/adaptive-card.json',
                        'type': 'AdaptiveCard',
                        'version': '1.0',
                        'body': [
                            {
                                'type': 'Container',
                                'speak': '<s>Hello There!</s><s>Below is the link for complete holiday list.</s>',
                                'items': [
                                    {
                                        'type': 'ColumnSet',
                                        'columns': [
                                            {
                                                'type': 'Column',
                                                'size': 'auto',
                                                'items': [
                                                    {
                                                        'type': 'Image',
                                                        'url': 'https://mjkretsinger.com/wp-content/uploads/2014/01/client-logo-deluxe.jpg',
                                                        'size': 'medium',
                                                        'style': 'person'
                                                    }
                                                ]
                                            },
                                            {
                                                'type': 'Column',
                                                'size': 'stretch',
                                                'items': [
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Hello There!',
                                                        'weight': 'bolder',
                                                        'isSubtle': true
                                                    },
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Below is the link for complete holiday list.',
                                                        'wrap': true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        'actions': [
                            
                            {
                                'type': 'Action.ShowCard',
                                'title': 'Complete Holiday List',
                                'speak': '<s>Complete Holiday List</s>',
                                'card': {
                                    'type': 'AdaptiveCard',
                                    'body': [],                           
                                    'actions': [
                                        {
                                            'type': 'Action.OpenUrl',
                                            'title': 'Link to Complete Holiday List',
                                            "url": "https://deluxe.atlassian.net/wiki/display/FSDEV/calendar/ac1c53a4-85a2-4519-b651-db74940bfee3?calendarName=Holidays"
                                        }
                                    ]
                                }
                            }    
                        ]
                    }
                };
                var msg = new builder.Message(session)
                    .addAttachment(card);
                session.send(msg);
                break;
                case 'Other CheckLists':
                var card = {
                    'contentType': 'application/vnd.microsoft.card.adaptive',
                    'content': {
                        '$schema': 'http://adaptivecards.io/schemas/adaptive-card.json',
                        'type': 'AdaptiveCard',
                        'version': '1.0',
                        'body': [
                            {
                                'type': 'Container',
                                'speak': '<s>Hello There!</s><s>Below List will help you know more.</s>',
                                'items': [
                                    {
                                        'type': 'ColumnSet',
                                        'columns': [
                                            {
                                                'type': 'Column',
                                                'size': 'auto',
                                                'items': [
                                                    {
                                                        'type': 'Image',
                                                        'url': 'https://mjkretsinger.com/wp-content/uploads/2014/01/client-logo-deluxe.jpg',
                                                        'size': 'medium',
                                                        'style': 'person'
                                                    }
                                                ]
                                            },
                                            {
                                                'type': 'Column',
                                                'size': 'stretch',
                                                'items': [
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Hello There!',
                                                        'weight': 'bolder',
                                                        'isSubtle': true
                                                    },
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Below List will help you know more.',
                                                        'wrap': true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        'actions': [
                            {
                                'type': 'Action.ShowCard',
                                'title': 'Team Lead CheckLists',
                                'speak': '<s>Team Lead CheckLists</s>',
                                'card': {
                                    'type': 'AdaptiveCard',
                                    'body': [],
                                    'actions': [
                                        {
                                            'type': 'Action.ShowCard',
                                            'title': 'Team Lead Checklist(Agile Practice)',
                                            'speak': '<s>Team Lead Checklist(Agile Practice)</s>',
                                            'card': {
                                            'type': 'AdaptiveCard',
                                            'body': [],
                                            'actions': [
                                                         {
                                                            'type': 'Action.OpenUrl',
                                                            'title': 'Link to Team Lead Checklist(Agile Practice)',
                                                            "url":"https://deluxe.atlassian.net/wiki/spaces/FSDEV/pages/236749094/Team+Lead+Checklist+Agile+Practice"
                                                         }
                                                         ]
                                             }
                                            
                                             },
                                             {
                                                'type': 'Action.ShowCard',
                                                'title': 'Team Lead Checklist(DevOps)',
                                                'speak': '<s>Team Lead Checklist(DevOps)</s>',
                                                'card': {
                                                'type': 'AdaptiveCard',
                                                'body': [],
                                                'actions': [
                                                             {
                                                                'type': 'Action.OpenUrl',
                                                                'title': 'Link to Team Lead Checklist(DevOps)',
                                                                "url":"https://deluxe.atlassian.net/wiki/spaces/FSDEV/pages/267190915/Team+Lead+Checklist+DevOps"
                                                             }
                                                             ]
                                                 }
                                                
                                                 },
                                                 {
                                                    'type': 'Action.ShowCard',
                                                    'title': 'Team Lead Checklist(UX Team)',
                                                    'speak': '<s>Link to Team Lead Checklist(UX Team)</s>',
                                                    'card': {
                                                    'type': 'AdaptiveCard',
                                                    'body': [],
                                                    'actions': [
                                                                 {
                                                                    'type': 'Action.OpenUrl',
                                                                    'title': 'Team Lead Checklist(UX Team)',
                                                                    "url":"https://deluxe.atlassian.net/wiki/spaces/FSDEV/pages/75628905/Team+Lead+Checklist+UX+Team"
                                                                    
                                                                 }
                                                                 ]
                                                            }
                                                    
                                                     },
                                                     {
                                                        'type': 'Action.ShowCard',
                                                        'title': 'Termination Checklist(QA/Development Manager List)',
                                                        'speak': '<s>Link to Termination Checklist(QA/Development Manager List)</s>',
                                                        'card': {
                                                        'type': 'AdaptiveCard',
                                                        'body': [],
                                                        'actions': [
                                                                     {
                                                                        'type': 'Action.OpenUrl',
                                                                        'title': 'Termination Checklist(QA/Development Manager List)',
                                                                        "url":"https://deluxe.atlassian.net/wiki/spaces/FSDEV/pages/147750913/Termination+Checklist+Development+QA+Engineer+-+Manager+Checklist"
                                                                     }
                                                                     ]
                                                                }
                                                        
                                                         }        
                                            ]
                                }
                            }
                        ]
                    }
                };
                
                var msg = new builder.Message(session)
                    .addAttachment(card);
                session.send(msg); 
                break; 
                case 'Benefits - Medical &Dental':
                   var card = {
                    'contentType': 'application/vnd.microsoft.card.adaptive',
                    'content': {
                        '$schema': 'http://adaptivecards.io/schemas/adaptive-card.json',
                        'type': 'AdaptiveCard',
                        'version': '1.0',
                        'body': [
                            {
                                'type': 'Container',
                                'speak': '<s>Hello There!</s><s>Below is the link to know more about Benefits - Medical &Dental</s>',
                                'items': [
                                    {
                                        'type': 'ColumnSet',
                                        'columns': [
                                            {
                                                'type': 'Column',
                                                'size': 'auto',
                                                'items': [
                                                    {
                                                        'type': 'Image',
                                                        'url': 'https://mjkretsinger.com/wp-content/uploads/2014/01/client-logo-deluxe.jpg',
                                                        'size': 'medium',
                                                        'style': 'person'
                                                    }
                                                ]
                                            },
                                            {
                                                'type': 'Column',
                                                'size': 'stretch',
                                                'items': [
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Hello There!',
                                                        'weight': 'bolder',
                                                        'isSubtle': true
                                                    },
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Below is the link to know more about Benefits - Medical &Dental',
                                                        'wrap': true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        'actions': [
                            
                            {
                                'type': 'Action.ShowCard',
                                'title': 'Benefits - Medical &Dental',
                                'speak': '<s>Benefits - Medical &Dental</s>',
                                'card': {
                                    'type': 'AdaptiveCard',
                                    'body': [],                           
                                    'actions': [
                                        {
                                            'type': 'Action.OpenUrl',
                                            'title': 'Link to Benefits - Medical &Dental',
                                            "url": "http://inside.deluxe.com/resources/home/InfoCenter/BenefitsDLX/Home.aspx"
                                        }
                                    ]
                                }
                            }    
                        ]
                    }
                };
                var msg = new builder.Message(session)
                    .addAttachment(card);
                session.send(msg);
                break; 
                case 'Apply for Leave request':
                   var card = {
                    'contentType': 'application/vnd.microsoft.card.adaptive',
                    'content': {
                        '$schema': 'http://adaptivecards.io/schemas/adaptive-card.json',
                        'type': 'AdaptiveCard',
                        'version': '1.0',
                        'body': [
                            {
                                'type': 'Container',
                                'speak': '<s>Hello There!</s><s>Below is the link Apply for Leave request</s>',
                                'items': [
                                    {
                                        'type': 'ColumnSet',
                                        'columns': [
                                            {
                                                'type': 'Column',
                                                'size': 'auto',
                                                'items': [
                                                    {
                                                        'type': 'Image',
                                                        'url': 'https://mjkretsinger.com/wp-content/uploads/2014/01/client-logo-deluxe.jpg',
                                                        'size': 'medium',
                                                        'style': 'person'
                                                    }
                                                ]
                                            },
                                            {
                                                'type': 'Column',
                                                'size': 'stretch',
                                                'items': [
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Hello There!',
                                                        'weight': 'bolder',
                                                        'isSubtle': true
                                                    },
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Below is the link to Apply for Leave request',
                                                        'wrap': true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        'actions': [
                            
                            {
                                'type': 'Action.ShowCard',
                                'title': 'Apply for Leave request',
                                'speak': '<s>Apply for Leave request</s>',
                                'card': {
                                    'type': 'AdaptiveCard',
                                    'body': [],                           
                                    'actions': [
                                        {
                                            'type': 'Action.OpenUrl',
                                            'title': 'Login into this link to Apply for Leave request',
                                            "url": "http://sap-web.deluxe.com:8160/zdlx/its/zh15"
                                        }
                                    ]
                                }
                            }    
                        ]
                    }
                };
                var msg = new builder.Message(session)
                    .addAttachment(card);
                session.send(msg);
                break; 
                case 'How to participate in 401K plant':
                   var card = {
                    'contentType': 'application/vnd.microsoft.card.adaptive',
                    'content': {
                        '$schema': 'http://adaptivecards.io/schemas/adaptive-card.json',
                        'type': 'AdaptiveCard',
                        'version': '1.0',
                        'body': [
                            {
                                'type': 'Container',
                                'speak': '<s>Hello There!</s><s>Below is the link to see how to participate in 401K plan</s>',
                                'items': [
                                    {
                                        'type': 'ColumnSet',
                                        'columns': [
                                            {
                                                'type': 'Column',
                                                'size': 'auto',
                                                'items': [
                                                    {
                                                        'type': 'Image',
                                                        'url': 'https://mjkretsinger.com/wp-content/uploads/2014/01/client-logo-deluxe.jpg',
                                                        'size': 'medium',
                                                        'style': 'person'
                                                    }
                                                ]
                                            },
                                            {
                                                'type': 'Column',
                                                'size': 'stretch',
                                                'items': [
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Hello There!',
                                                        'weight': 'bolder',
                                                        'isSubtle': true
                                                    },
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Below is the link to see how to participate in 401K plan',
                                                        'wrap': true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        'actions': [
                            
                            {
                                'type': 'Action.ShowCard',
                                'title': 'How to participate in 401K plan',
                                'speak': '<s>How to participate in 401K plan</s>',
                                'card': {
                                    'type': 'AdaptiveCard',
                                    'body': [],                           
                                    'actions': [
                                        {
                                            'type': 'Action.OpenUrl',
                                            'title': 'Link to see how to participate in 401K plan',
                                            "url": "http://inside.deluxe.com/resources/home/InfoCenter/SitePages/BenefitsMain.aspx"
                                        }
                                    ]
                                }
                            }    
                        ]
                    }
                };
                var msg = new builder.Message(session)
                    .addAttachment(card);
                session.send(msg);
                break;
                case 'View Paycheque Online':
                   var card = {
                    'contentType': 'application/vnd.microsoft.card.adaptive',
                    'content': {
                        '$schema': 'http://adaptivecards.io/schemas/adaptive-card.json',
                        'type': 'AdaptiveCard',
                        'version': '1.0',
                        'body': [
                            {
                                'type': 'Container',
                                'speak': '<s>Hello There!</s><s>Below is the link to see your Paycheque Online</s>',
                                'items': [
                                    {
                                        'type': 'ColumnSet',
                                        'columns': [
                                            {
                                                'type': 'Column',
                                                'size': 'auto',
                                                'items': [
                                                    {
                                                        'type': 'Image',
                                                        'url': 'https://mjkretsinger.com/wp-content/uploads/2014/01/client-logo-deluxe.jpg',
                                                        'size': 'medium',
                                                        'style': 'person'
                                                    }
                                                ]
                                            },
                                            {
                                                'type': 'Column',
                                                'size': 'stretch',
                                                'items': [
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Hello There!',
                                                        'weight': 'bolder',
                                                        'isSubtle': true
                                                    },
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Below is the link to see your Paycheque Online',
                                                        'wrap': true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        'actions': [
                            
                            {
                                'type': 'Action.ShowCard',
                                'title': 'View Paycheque Online',
                                'speak': '<s>View Paycheque Online</s>',
                                'card': {
                                    'type': 'AdaptiveCard',
                                    'body': [],                           
                                    'actions': [
                                        {
                                            'type': 'Action.OpenUrl',
                                            'title': 'Link to see your Paycheque Online',
                                            "url": "http://sap-web.deluxe.com:8160/zdlx/its/zh15"
                                        }
                                    ]
                                }
                            }    
                        ]
                    }
                };
                var msg = new builder.Message(session)
                    .addAttachment(card);
                session.send(msg);
                break; 
                case 'Enroll in stock purchase plan':
                   var card = {
                    'contentType': 'application/vnd.microsoft.card.adaptive',
                    'content': {
                        '$schema': 'http://adaptivecards.io/schemas/adaptive-card.json',
                        'type': 'AdaptiveCard',
                        'version': '1.0',
                        'body': [
                            {
                                'type': 'Container',
                                'speak': '<s>Hello There!</s><s>Below is the link to Enroll in stock purchase plan</s>',
                                'items': [
                                    {
                                        'type': 'ColumnSet',
                                        'columns': [
                                            {
                                                'type': 'Column',
                                                'size': 'auto',
                                                'items': [
                                                    {
                                                        'type': 'Image',
                                                        'url': 'https://mjkretsinger.com/wp-content/uploads/2014/01/client-logo-deluxe.jpg',
                                                        'size': 'medium',
                                                        'style': 'person'
                                                    }
                                                ]
                                            },
                                            {
                                                'type': 'Column',
                                                'size': 'stretch',
                                                'items': [
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Hello There!',
                                                        'weight': 'bolder',
                                                        'isSubtle': true
                                                    },
                                                    {
                                                        'type': 'TextBlock',
                                                        'text': 'Below is the link to Enroll in stock purchase plan',
                                                        'wrap': true
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        'actions': [
                            
                            {
                                'type': 'Action.ShowCard',
                                'title': 'Enroll in stock purchase plan',
                                'speak': '<s>Enroll in stock purchase plan</s>',
                                'card': {
                                    'type': 'AdaptiveCard',
                                    'body': [],                           
                                    'actions': [
                                        {
                                            'type': 'Action.OpenUrl',
                                            'title': 'Link to Enroll in stock purchase plan',
                                            "url": "https://www.shareowneronline.com/UserManagement/WFIndex.aspx"
                                        }
                                    ]
                                }
                            }    
                        ]
                    }
                };
                var msg = new builder.Message(session)
                    .addAttachment(card);
                session.send(msg);
                break;                           
             }
        
            }
]

