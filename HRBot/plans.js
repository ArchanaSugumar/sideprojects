var builder = require('botbuilder');
module.exports=[
function(session){
    var card = {
        'contentType': 'application/vnd.microsoft.card.adaptive',
        'content': {
            '$schema': 'http://adaptivecards.io/schemas/adaptive-card.json',
            'type': 'AdaptiveCard',
            'version': '1.0',
            'body': [
                {
                    'type': 'Container',
                    'speak': '<s>Hello There!</s><s>Below is the link to see how to participate in 401K plan</s>',
                    'items': [
                        {
                            'type': 'ColumnSet',
                            'columns': [
                                {
                                    'type': 'Column',
                                    'size': 'auto',
                                    'items': [
                                        {
                                            'type': 'Image',
                                            'url': 'https://mjkretsinger.com/wp-content/uploads/2014/01/client-logo-deluxe.jpg',
                                            'size': 'medium',
                                            'style': 'person'
                                        }
                                    ]
                                },
                                {
                                    'type': 'Column',
                                    'size': 'stretch',
                                    'items': [
                                        {
                                            'type': 'TextBlock',
                                            'text': 'Hello There!',
                                            'weight': 'bolder',
                                            'isSubtle': true
                                        },
                                        {
                                            'type': 'TextBlock',
                                            'text': 'Below is the link to see how to participate in 401K plan',
                                            'wrap': true
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            'actions': [
                
                {
                    'type': 'Action.ShowCard',
                    'title': 'How to participate in 401K plan',
                    'speak': '<s>How to participate in 401K plan</s>',
                    'card': {
                        'type': 'AdaptiveCard',
                        'body': [],                           
                        'actions': [
                            {
                                'type': 'Action.OpenUrl',
                                'title': 'Link to see how to participate in 401K plan',
                                "url": "http://inside.deluxe.com/resources/home/InfoCenter/SitePages/BenefitsMain.aspx"
                            }
                        ]
                    }
                }    
            ]
        }
    };
    var msg = new builder.Message(session)
        .addAttachment(card);
    session.send(msg);
}];
