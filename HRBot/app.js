var builder = require('botbuilder');
var restify = require('restify');
var url = require('url');
var fs = require('fs');
var util = require('util');

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url);
});

//var appId = "<MICROSOFT_APP_ID>";
//var appPassword = "<MICROSOFT_APP_PASSWORD>";

var connector = new builder.ChatConnector();

server.post('/api/messages', connector.listen());
var inMemoryStorage = new builder.MemoryBotStorage();
var bot = new builder.UniversalBot(connector);
var luisAppId = "5b6ac2a8-f6b1-4f23-b05f-7cef6992a464";
var luisAPIKey = "10c1e317a07a44408d63f464de36e8a5";
var luisAPIHostName = "westus.api.cognitive.microsoft.com";

const luisModelUrl = 'https://' + luisAPIHostName + '/luis/v2.0/apps/' + luisAppId + '?subscription-key=' + luisAPIKey;
var recognizer = new builder.LuisRecognizer(luisModelUrl);
var intents = new builder.IntentDialog({
    recognizers: [recognizer]
});

bot.dialog('/', intents);
intents.matches('UserGreet', (session, args, next) => {
    session.send("Happy to help. Please let me know if you have other queries");
    
});
intents.matches('Greet', require('./Greet.js'));
intents.matches('NewJoin', require('./NewJoin.js'));
intents.matches('TeamInfo', require('./TeamInfo.js'));
intents.matches('agile_P', require('./agile_P.js'));
intents.matches('Dev', require('./dev.js'));
intents.matches('ux', require('./ux.js'));
intents.matches('devqa', require('./devqa.js'));
intents.matches('Other CheckLists', require('./OtherChecklist.js'));
intents.matches('Holiday', require('./holiday.js'));
intents.matches('medical', require('./medical.js'));
intents.matches('plans', require('./plans.js'));
intents.matches('stock', require('./stock.js'));
intents.matches('paycheck', require('./paycheck.js'));
intents.matches('leaverequest', require('./leaverequest.js'));
intents.matches('None', require('./None.js'));
